package com.example.material_electoral.vo

import com.example.material_electoral.domain.WebService
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitClient {

    val webservice by lazy {
        Retrofit.Builder()
            .baseUrl("http://192.168.137.1:5000/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(WebService::class.java)
    }

}