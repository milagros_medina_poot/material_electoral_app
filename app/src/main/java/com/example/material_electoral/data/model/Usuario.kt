package com.example.material_electoral.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Usuario (
    @SerializedName("id")
    val id:String = "",
    @SerializedName("email")
    val email:String = "",
    @SerializedName("password")
    val password:String = "",
    @SerializedName("nombre")
    val nombre:String = "",
    @SerializedName("rol")
    val rol:String = "",
    @SerializedName("token")
    val token:String = ""
): Parcelable