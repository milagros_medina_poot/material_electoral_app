package com.example.material_electoral.data.request

data class CaelRequest (
    val nombre: String,
    val area_responsabilidad: String,
    val casillas_totales: Int,
    val casillas_entregados: Int,
    val observaciones: String
    )