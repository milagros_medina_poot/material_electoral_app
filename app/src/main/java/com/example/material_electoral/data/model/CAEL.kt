package com.example.material_electoral.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CAEL(
    @SerializedName("id")
    val caelsId:Int = 0,
    @SerializedName("nombre")
    val caelsNombre:String = "",
    @SerializedName("area_responsabilidad")
    val area_responsabilidad:String = "",
    @SerializedName("casillas_totales")
    val casillas_totales:Int = 0,
    @SerializedName("casillas_entregados")
    val casillas_entregados:Int = 0,
    @SerializedName("observaciones")
    val observaciones:String = "",
    ):Parcelable

data class CaelsList(
    @SerializedName("caels")
    val caelsList:List<CAEL> = listOf()
)