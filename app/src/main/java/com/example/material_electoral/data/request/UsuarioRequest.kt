package com.example.material_electoral.data.request

data class UsuarioRequest(
    val email: String,
    val password: String
)