package com.example.material_electoral.data.response

import com.google.gson.annotations.SerializedName

data class CaelResponse (
        @SerializedName("Agregado")
        val agregado: String,
        @SerializedName("Nombre")
        val nombre: String
)