package com.example.material_electoral.data

import com.example.material_electoral.data.model.CAEL
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.data.request.CaelRequest
import com.example.material_electoral.data.request.UsuarioRequest
import com.example.material_electoral.data.response.CaelResponse
import com.example.material_electoral.vo.Resource
import com.example.material_electoral.vo.RetrofitClient

class DataSource {

    suspend fun getUsuario(usuario: UsuarioRequest): Resource<Usuario>{
        return Resource.Success(RetrofitClient.webservice.getUsuario(usuario))
    }

    suspend fun getCaelsList(): Resource<List<CAEL>>{
        return Resource.Success(RetrofitClient.webservice.getCaels().caelsList)
    }

    suspend fun registrarCael(cael: CaelRequest): Resource<CaelResponse>{
        return Resource.Success(RetrofitClient.webservice.registrarCael(cael))
    }
}