package com.example.material_electoral.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.material_electoral.R
import com.example.material_electoral.base.BaseViewHolder
import com.example.material_electoral.data.model.CAEL
import com.example.material_electoral.databinding.CaelRvItemBinding


class RVAdapter (private val context: Context, private val caelsList:List<CAEL>,
                 private val itemCLickListener: OnCaelClickListener) :
RecyclerView.Adapter<BaseViewHolder<*>>(){

    interface OnCaelClickListener{
        fun onCaelClick(cael: CAEL, position: Int)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return MainViewHolder(LayoutInflater.from(context).inflate(R.layout.cael_rv_item, parent, false))
    }

    override fun getItemCount(): Int {
        return caelsList.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder){
            is MainViewHolder -> holder.bind(caelsList[position], position)
        }
    }

    inner class MainViewHolder(itemView: View): BaseViewHolder<CAEL>(itemView){
        val binding = CaelRvItemBinding.bind(itemView)

        override fun bind(item: CAEL, position: Int) {
            binding.txtNombre.text = item.caelsNombre
            itemView.setOnClickListener { itemCLickListener.onCaelClick(item, position) }
        }

    }

}