package com.example.material_electoral.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.material_electoral.MainActivity
import com.example.material_electoral.data.DataSource
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.databinding.FragmentLoginBinding
import com.example.material_electoral.domain.RepoImpl
import com.example.material_electoral.ui.viewmodel.LoginViewModel
import com.example.material_electoral.ui.viewmodel.VMFactory
import com.example.material_electoral.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


val Context.dataStore by preferencesDataStore(name = "dataStore")

class LoginFragment : Fragment() {

    private val viewModel by viewModels<LoginViewModel>{ VMFactory(RepoImpl(DataSource())) }

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()

        binding.loginBtn.setOnClickListener {
            Log.e("Email",binding.emailInput.text.toString())
            Log.e("Password",binding.passwordInput.text.toString())

            viewModel.setUsuario(binding.emailInput.text.toString(), binding.passwordInput.text.toString())
            setupObservers()
        }
    }

    private fun setupObservers(){
        viewModel.fetchUsuarios.observe(viewLifecycleOwner, Observer { result ->
            when(result){
                is Resource.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressBar.visibility = View.GONE
                    Log.e("Result", result.data.toString())

                    lifecycleScope.launch(Dispatchers.IO) {
                        saveDataUser(result.data)
                    }

                    startActivity(Intent(activity, MainActivity::class.java))
                    activity?.finish()


                }
                is Resource.Failure -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), "Ocurrió un error al traer los datos ${result.exception}", Toast.LENGTH_SHORT).show()
                    Log.w("Error", result.exception)
                }
            }
        })
    }

    private suspend fun saveDataUser(usuario: Usuario) {
        Log.e("usuario savedatauser", usuario.toString())
        context?.dataStore?.edit { preferences ->
            preferences[stringPreferencesKey(("id_usuario"))] = usuario.id
            preferences[stringPreferencesKey(("email_usuario"))] = usuario.email
            preferences[stringPreferencesKey(("password_usuario"))] = usuario.password
            preferences[stringPreferencesKey("nombre_usuario")] = usuario.nombre
            preferences[stringPreferencesKey(("rol_usuario"))] = usuario.rol
            preferences[stringPreferencesKey(("token_usuario"))] = usuario.token
        }
    }


}