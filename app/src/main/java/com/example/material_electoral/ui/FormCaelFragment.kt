package com.example.material_electoral.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.material_electoral.R
import com.example.material_electoral.data.DataSource
import com.example.material_electoral.data.request.CaelRequest
import com.example.material_electoral.databinding.FragmentCaelsBinding
import com.example.material_electoral.databinding.FragmentFormCaelBinding
import com.example.material_electoral.domain.RepoImpl
import com.example.material_electoral.ui.viewmodel.CaelsViewModel
import com.example.material_electoral.ui.viewmodel.LoginViewModel
import com.example.material_electoral.ui.viewmodel.VMFactory
import com.example.material_electoral.vo.Resource

class FormCaelFragment : Fragment() {

    private val viewModel by viewModels<CaelsViewModel>{ VMFactory(RepoImpl(DataSource())) }

    private var _binding: FragmentFormCaelBinding? = null
    private val binding get() = _binding!!

    private lateinit var cael: CaelRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFormCaelBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.casillasTotales.minValue = 0
        binding.casillasTotales.maxValue = 10
        binding.casillasEntregadas.minValue = 0
        
        binding.casillasTotales.setOnValueChangedListener { numberPicker, i, i2 ->  binding.casillasEntregadas.maxValue = i2}

        binding.saveBtn.setOnClickListener {
            cael = CaelRequest(binding.nombreInput.text.toString(),
                binding.areaInput.text.toString(),
                binding.casillasTotales.value,
                binding.casillasEntregadas.value,
                binding.obsInput.text.toString())
            setupObservers()
        }
    }

    private fun setupObservers(){
        viewModel.registrarCael(cael).observe(viewLifecycleOwner, Observer { result->
            when(result){
                is Resource.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), "Nuevo cael: ${result.data}", Toast.LENGTH_SHORT).show()
                    Log.d("Cael", "${result.data}")
                }
                is Resource.Failure -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), "Ocurrió un error al registrar los datos ${result.exception}", Toast.LENGTH_SHORT).show()
                    Log.w("Error", result.exception)
                }
            }
        })
    }

}