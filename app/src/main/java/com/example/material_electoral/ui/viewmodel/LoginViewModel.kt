package com.example.material_electoral.ui.viewmodel

import android.content.Context
import android.util.Log
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.data.request.UsuarioRequest
import com.example.material_electoral.domain.Repo
import com.example.material_electoral.ui.dataStore
import com.example.material_electoral.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class LoginViewModel(private val repo: Repo):ViewModel(){

    private val usuarioData = MutableLiveData<UsuarioRequest>()

    fun setUsuario(email: String, password: String){
        usuarioData.value = UsuarioRequest(email, password)
    }

    val fetchUsuarios = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.getUsuario(UsuarioRequest(usuarioData.value?.email.toString(), usuarioData.value?.password.toString())))
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }

}