package com.example.material_electoral.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.datastore.dataStore
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.material_electoral.MainActivity
import com.example.material_electoral.R
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.databinding.FragmentMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //metodos

        lifecycleScope.launch(Dispatchers.IO) {
            getDataUsuario()?.collect{
                withContext(Dispatchers.Main){
                    Log.e("usuario getdatausuario", it.toString())
                    binding.txtUsuario.text = it.nombre
                }
            }
        }

        //binding.txtNombreUsuario.text = (activity as MainActivity).usuario.nombre

        binding.btnIrCaels.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_caelsFragment)
        }
    }

    private fun getDataUsuario() = context?.dataStore?.data?.map {
        Usuario(
            it[stringPreferencesKey("id_usuario")].orEmpty(),
            it[stringPreferencesKey("email_usuario")].orEmpty(),
            it[stringPreferencesKey("password_usuario")].orEmpty(),
            it[stringPreferencesKey("nombre_usuario")].orEmpty(),
            it[stringPreferencesKey("rol_usuario")].orEmpty(),
            it[stringPreferencesKey("token_usuario")].orEmpty()
        )
    }


}