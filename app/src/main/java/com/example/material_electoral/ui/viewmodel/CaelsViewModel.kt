package com.example.material_electoral.ui.viewmodel

import androidx.lifecycle.*
import com.example.material_electoral.data.request.CaelRequest
import com.example.material_electoral.domain.Repo
import com.example.material_electoral.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CaelsViewModel (private val repo: Repo): ViewModel(){

    private val caelsData = MutableLiveData<String>()

    fun getCaelsList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.getCaelsList())
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }

    fun registrarCael(cael: CaelRequest) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.registrarCael(cael))
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }

}