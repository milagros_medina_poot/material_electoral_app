package com.example.material_electoral.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.material_electoral.R
import com.example.material_electoral.data.DataSource
import com.example.material_electoral.data.model.CAEL
import com.example.material_electoral.databinding.FragmentCaelsBinding
import com.example.material_electoral.domain.RepoImpl
import com.example.material_electoral.ui.viewmodel.CaelsViewModel
import com.example.material_electoral.ui.viewmodel.LoginViewModel
import com.example.material_electoral.ui.viewmodel.VMFactory
import com.example.material_electoral.vo.Resource


class CaelsFragment : Fragment(), RVAdapter.OnCaelClickListener{

    private val viewModel by viewModels<CaelsViewModel>{ VMFactory(RepoImpl(DataSource())) }

    private var _binding:FragmentCaelsBinding? = null
    private val binding get() = _binding!!

    private lateinit var caelsMutableList: MutableList<CAEL>
    private lateinit var adapter:RVAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCaelsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        setupObservers()

        binding.fabAdd.setOnClickListener {
            findNavController().navigate(R.id.action_caelsFragment_to_formCaelFragment)
        }
    }

    private fun setupObservers(){
        viewModel.getCaelsList().observe(viewLifecycleOwner, Observer { result->
            when(result){
                is Resource.Loading -> {}
                is Resource.Success -> {
                    binding.rvCaels.adapter = RVAdapter(requireContext(),result.data, this)

                    /*adapter = RVAdapter(requireContext(), caelsMutableList, this)
                    caelsMutableList = result.data.map{
                        CAEL(it.caelsId, it.caelsNombre)
                    }.toMutableList()

                    binding.rvCaels.adapter = adapter*/
                    Log.d("LISTA CAELs", "${result.data}")
                }
                is Resource.Failure -> {

                }
            }
        })
    }

    private fun setupRecyclerView(){
        binding.rvCaels.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onCaelClick(cael: CAEL, position: Int) {
        Toast.makeText(activity, cael.caelsId.toString() +"-"+ cael.caelsNombre, Toast.LENGTH_LONG).show()
    }
}