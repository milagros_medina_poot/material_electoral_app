package com.example.material_electoral.domain

import com.example.material_electoral.data.model.CAEL
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.data.request.CaelRequest
import com.example.material_electoral.data.request.UsuarioRequest
import com.example.material_electoral.data.response.CaelResponse
import com.example.material_electoral.vo.Resource

interface Repo {
    suspend fun getUsuario(usuario: UsuarioRequest): Resource<Usuario>

   suspend fun getCaelsList(): Resource<List<CAEL>>

   suspend fun registrarCael(cael: CaelRequest): Resource<CaelResponse>
}