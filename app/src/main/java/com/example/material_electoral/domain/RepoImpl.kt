package com.example.material_electoral.domain

import com.example.material_electoral.data.DataSource
import com.example.material_electoral.data.model.CAEL
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.data.request.CaelRequest
import com.example.material_electoral.data.request.UsuarioRequest
import com.example.material_electoral.data.response.CaelResponse
import com.example.material_electoral.vo.Resource

class RepoImpl(private val dataSource:DataSource): Repo {

    suspend override fun getUsuario(usuario: UsuarioRequest): Resource<Usuario> {
        return dataSource.getUsuario(usuario)
    }

    override suspend fun getCaelsList():Resource<List<CAEL>>{
        return dataSource.getCaelsList()
    }

    override suspend fun registrarCael(cael: CaelRequest): Resource<CaelResponse> {
        return dataSource.registrarCael(cael)
    }

}