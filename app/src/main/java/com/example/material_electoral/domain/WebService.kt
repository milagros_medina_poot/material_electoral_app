package com.example.material_electoral.domain

import com.example.material_electoral.data.model.CaelsList
import com.example.material_electoral.data.model.Usuario
import com.example.material_electoral.data.request.CaelRequest
import com.example.material_electoral.data.request.UsuarioRequest
import com.example.material_electoral.data.response.CaelResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface WebService {

    @POST("login")
    suspend fun getUsuario(@Body usuario: UsuarioRequest): Usuario

    @GET("caels")
    suspend fun getCaels(): CaelsList

    @POST("registrarCael")
    suspend fun registrarCael(@Body cael: CaelRequest): CaelResponse
}